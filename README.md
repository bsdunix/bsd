# README #

This repo is a collection of shell scripts to configure your FreeBSD install. I decided to name the repo for BSD due to potential interests down the road to explore other BSD siblings such as OpenBSD and NetBSD.